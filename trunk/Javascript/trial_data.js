/*
Ace Attorney Online - Functions to generate data about a trial

*/

//MODULE DESCRIPTOR
Modules.load(new Object({
	name : 'trial_data',
	dependencies : ['trial'],
	init : function() {}
}));

//INDEPENDENT INSTRUCTIONS

//EXPORTED VARIABLES

//EXPORTED FUNCTIONS
function getNewId(array, first_index)
{
	var new_id = 0;
	for(var i = !isNaN(first_index) ? first_index : 1; i < array.length; i++)
	{
		new_id = Math.max(new_id, array[i].id);
	}
	
	return new_id + 1;
}

function getNewRowId(type)
{
	return getNewId(trial_data[type]);
}

function insertDataRow(type, position, new_row)
{
	var data_table = trial_data[type];
	
	//TODO remove debugging calls
	if(isNaN(new_row.id)) { debugger; }
	if(position === 0){ debugger; }
	
	if(!position && position !== 0)
	{
		//if position is undefined or non numeric, insert at end
		data_table.push(new_row);
	}
	else
	{
		//else, insert at given position
		data_table.splice(position, 0, new_row);
	}
}

function createDataRow(type, given_id)
{
	switch(type)
	{
		case 'frames' :
			
			return new Object({
				id: given_id || getNewRowId(type),
				speaker_name: "",
				speaker_use_name: false,
				speaker_id: PROFILE_EMPTY,
				speaker_voice: VOICE_AUTO,
				sound: SOUND_NONE,
				music: MUSIC_NONE,
				place: PLACE_NONE,
				place_position: POSITION_NONE,
				place_transition: TRANSITION_NO,
				characters: [],
				characters_erase_previous: false,
				popups: [],
				action_name: "",
				action_parameters: [],
				text_colour: "white",
				text_content: "",
				text_speed: 1,
				hidden: false,
				wait_time: 0,
				merged_to_next: false
			});
			
			break;
		
		case 'profiles' :
			
			return new Object({
				id: given_id || getNewRowId(type),
				long_name: "",
				short_name: "",
				description: "",
				civil_status:"",
				hidden: false,
				base:"Inconnu",
				icon:"",
				custom_sprites:[],
				voice:-1,
				auto_place_position:"",
				auto_place:"",
				auto_colour:""
			});
			
			break;
		
		case 'evidence' :
		
			return new Object({
				id: given_id || getNewRowId(type),
				name:"",
				description:"",
				metadata:"",
				hidden: false,
				icon:"",
				icon_external: false,
				check_button_data:[]
			});
			
			break;
		
		case 'sprite' :
			
			return new Object({
				id: given_id,
				name: given_id,
				talking: '',
				still: '',
				startup: '',
				startup_duration:0
			});
		
		case 'character_info' :
			
			return new Object({
				profile_id: given_id,
				sprite_id: 0,
				sync_mode: SYNC_AUTO,
				startup_mode: STARTUP_SKIP,
				position: POSITION_NONE, 
				mirror_effect: false,
				visual_effect_appears: EFFECT_FADING,
				visual_effect_appears_mode: 0, //automatic : trigger animation under system defined conditions
				visual_effect_disappears: EFFECT_FADING, 
				visual_effect_disappears_mode: 0
			});
			
			break;
		
		case 'popup_info' :
			
			return new Object({
				popup_id: 0,
				position: POSITION_NONE, 
				mirror_effect: false
			});
			
			break;
		
		case 'places' :
			
			return new Object({
				id: given_id || getNewRowId(type),
				name: "",
				background: new Object({
					image:"",
					external: false,
					hidden: false
				}),
				positions:[],
				background_objects:[],
				foreground_objects:[]
			});
			
			break;
		
		case 'music' :
			
			return new Object({
				id: given_id || getNewRowId(type),
				name: "",
				path:"",
				external: false,
				volume:100,
				loop_start:0
			});
			
			break;
		
		case 'sounds' :
		
			return new Object({
				id: given_id || getNewRowId(type),
				name: "",
				path: "",
				external: false,
				volume:100
			});
			
			break;
			
		case 'popups' :
		
			return new Object({
				id: given_id || getNewRowId(type),
				name: "",
				path: "",
				external: false
			});
			
			break;
		
		case 'contradiction' :
			
			return new Object({
				contrad_elt: null,
				destination: 0
			});
			
			break;
		
		case 'lock_info':
			
			return new Object({
				id: given_id,
				type: 'jfa_lock',
				x: 0,
				y: 0
			});
			
			break;
	}
}

function createAndInsertDataRow(type, position)
{
	var new_row = createDataRow(type);
	insertDataRow(type, position, new_row);
	
	return new_row;
}

/*
Creates a data row based on a preset (a data row with null id) and inserts it.
*/
function createAndInsertDataRowFromPreset(type, position, preset)
{
	var new_row = objClone(preset);
	new_row.id = getNewRowId(type);
	insertDataRow(type, position, new_row);
	
	return new_row;
}

function deleteDataRow(type, position)
{
	var data_table = trial_data[type];
	
	data_table.splice(position, 1);
}

function deleteDataRowInterval(type, start_index, end_index)
{
	for(var i = end_index; i >= start_index; i--)
	{
		deleteDataRow(type, i);
	}
}

function moveDataRowInterval(type, start_index, end_index, target_index)
{
	var data_table = trial_data[type];
	
	var nb_moved_rows = 1 + end_index - start_index;
	var rows = data_table.splice(start_index, nb_moved_rows);
	if(target_index > start_index)
	{
		// If target index after the removed interval, shift to the new target index before inserting.
		target_index -= nb_moved_rows;
	}
	data_table.splice.apply(data_table, [target_index, 0].concat(rows));
}

//Get the index in array of the element that possesses a given ID
//Returns -1 if object not found
function getIndexById(array, id)
{
	if(isNaN(id))
	{
		return -1;
	}
	
	//Fix the modulo operation, broken in JS...
	function mod(X, Y) 
	{
		var t;
		t = X % Y;
		return t < 0 ? t + Y : t;
	}
	
	//Start the search around the id - if the trial is optimised, it should be near
	var length = array.length;
	var top = mod(id, length);
	var bottom = mod(id - 1, length);
	
	while(true)
	{		
		if(array[top] && array[top].id == id)
		{
			return top;
		}
		
		if(array[bottom] && array[bottom].id == id)
		{
			return bottom;
		}
		
		//If cursors made a complete loop, row not found.
		if(top == bottom || top == bottom - 1)
		{
			break;
		}
		
		//Move the cursors, staying in the right interval.
		top = mod(top + 1, length);
		bottom = mod(bottom - 1, length);
	}

	return -1;
}

//Get the element from a array having a given ID
function getById(array, id)
{
	return array[getIndexById(array, id)];
}

//Get the index in the trial data tables of the row that possesses a given ID
function getRowIndexById(type, id)
{
	return getIndexById(trial_data[type], id);
}

//Get the row that possesses a given ID
function getRowById(type, id)
{
	return getById(trial_data[type], id);
}

/*
Creation / Deletion of conversations 
*/
function createNewConversationAt(position, last_frame_properties, first_frame_properties)
{
	//Insert first conversation frame
	var first_frame = createAndInsertDataRow('frames', position);
	first_frame.wait_time = 1;
	
	// Set first frame properties
	if(first_frame_properties)
	{
		for(var prop_name in first_frame_properties)
		{
			first_frame[prop_name] = first_frame_properties[prop_name];
		}
	}
	
	//Insert last conversation frame
	var last_frame = createAndInsertDataRow('frames', position + 1);
	last_frame.wait_time = 1;
	
	// Set last frame properties
	if(last_frame_properties)
	{
		for(var prop_name in last_frame_properties)
		{
			last_frame[prop_name] = last_frame_properties[prop_name];
		}
	}
	
	// Build and return the conversation descriptor
	return new Object({
		start: first_frame.id,
		end: last_frame.id
	})
}

function deleteConversation(conv_descriptor)
{
	var first_frame = getRowIndexById('frames', conv_descriptor.start);
	var last_frame = getRowIndexById('frames', conv_descriptor.end);
	
	for(var i = last_frame; i >= first_frame; i--)
	{
		deleteDataRow('frames', i);
	}
}

/*
Creation of new blocks
*/

function createNewCrossExamination(position)
{
	// Add cocouncil_conversation frames
	var cocouncil_end_frame = createAndInsertDataRow('frames', position);
	var cocouncil_start_frame = createAndInsertDataRow('frames', position);
	
	// Add first frame
	var first_frame = createAndInsertDataRow('frames', position);
	
	var newCE = new Object({
		id: getNewRowId('cross_examinations'),
		start: first_frame.id,
		end: cocouncil_end_frame.id,
		cocouncil_start: cocouncil_start_frame.id,
		cocouncil_end: cocouncil_end_frame.id,
		statements: new Array(),
		failure_conv_start: 0,
		failure_conv_end: 0
	});
	
	trial_data.cross_examinations.push(newCE);
	
	first_frame.action_name = 'CEStart';
	first_frame.action_parameters = new Object();
	first_frame.wait_time = 1;
	
	cocouncil_start_frame.action_name = 'CEPause';
	cocouncil_start_frame.action_parameters = new Object();
	cocouncil_start_frame.wait_time = 1;
	
	cocouncil_end_frame.action_name = 'CERestart';
	cocouncil_end_frame.action_parameters = prefixRawParameters(new Object({
		context: new Object({
			ce_desc: newCE.id
		})
	}));
	cocouncil_end_frame.wait_time = 1;
}

function createNewDialogue(position, dialogue_descriptor)
{
	var dialogue_intro_first_frame = createAndInsertDataRow('frames', position);
	var dialogue_intro_hide_frame = createAndInsertDataRow('frames', position + 1);
	var dialogue_intro_last_frame = createAndInsertDataRow('frames', position + 2);
	var dialogue_menu_frame = createAndInsertDataRow('frames', position + 3);
	var dialogue_talk_frame = createAndInsertDataRow('frames', position + 4);
	var dialogue_present_frame = createAndInsertDataRow('frames', position + 5);
	var dialogue_present_others_first_frame = createAndInsertDataRow('frames', position + 6);
	var dialogue_present_others_return_frame = createAndInsertDataRow('frames', position + 7);
	var dialogue_end_frame = createAndInsertDataRow('frames', position + 8);
	
	var newDialogue = new Object({
		id: dialogue_descriptor.section_id,
		
		start: dialogue_intro_first_frame.id,
		main: dialogue_menu_frame.id,
		talk: dialogue_talk_frame.id,
		present: dialogue_present_frame.id,
		end: dialogue_end_frame.id,
		
		intro_start: dialogue_intro_first_frame.id,
		intro_end: dialogue_intro_last_frame.id,
		
		talk_topics: [],
		present_conversations: [new Object({
			elt: null,
			start: dialogue_present_others_first_frame.id,
			end: dialogue_present_others_return_frame.id
		})],
		locks: null
	});
	
	dialogue_intro_first_frame.wait_time = 1;
	dialogue_intro_first_frame.hidden = 1;
	dialogue_intro_first_frame.action_name = 'GoTo';
	dialogue_intro_first_frame.action_parameters = prefixRawParameters(new Object({
		global: new Object({
			target: dialogue_menu_frame.id
		})
	}));
	
	dialogue_intro_hide_frame.wait_time = 1;
	dialogue_intro_hide_frame.action_name = 'RevealFrame';
	dialogue_intro_hide_frame.action_parameters = prefixRawParameters(new Object({
		multiple: new Object({
			frame: [new Object({
					target: dialogue_intro_first_frame.id
			})]
		})
	}));
	
	dialogue_intro_last_frame.wait_time = 1;
	dialogue_intro_last_frame.action_name = 'GoTo';
	dialogue_intro_last_frame.action_parameters = prefixRawParameters(new Object({
		global: new Object({
			target: dialogue_menu_frame.id
		})
	}));
	
	dialogue_menu_frame.action_name = 'DialogueMenu';
	dialogue_menu_frame.action_parameters = prefixRawParameters(new Object({
		context: new Object({
			dialogue: dialogue_descriptor
		})
	}));
	
	dialogue_talk_frame.action_name = 'DialogueTalk';
	dialogue_talk_frame.action_parameters = prefixRawParameters(new Object({
		context: new Object({
			dialogue: dialogue_descriptor
		})
	}));
	
	dialogue_present_frame.action_name = 'DialoguePresent';
	dialogue_present_frame.action_parameters = prefixRawParameters(new Object({
		context: new Object({
			dialogue: dialogue_descriptor
		})
	}));
	
	dialogue_present_others_first_frame.wait_time = 1;
	
	dialogue_present_others_return_frame.wait_time = 1;
	dialogue_present_others_return_frame.action_name = 'GoTo';
	dialogue_present_others_return_frame.action_parameters = prefixRawParameters(new Object({
		global: new Object({
			target: dialogue_menu_frame.id
		})
	}));
	
	return newDialogue;
}

function createNewExamination(position, examination_descriptor)
{
	var examination_examine_frame = createAndInsertDataRow('frames', position);
	var examination_examine_others_first_frame = createAndInsertDataRow('frames', position + 1);
	var examination_examine_others_return_frame = createAndInsertDataRow('frames', position + 2);
	var examination_deduce_others_first_frame = createAndInsertDataRow('frames', position + 3);
	var examination_deduce_others_return_frame = createAndInsertDataRow('frames', position + 4);
	var examination_end_frame = createAndInsertDataRow('frames', position + 5);
	
	var newExamination = new Object({
		id: examination_descriptor.section_id,
		
		start: examination_examine_frame.id,
		examine: examination_examine_frame.id,
		end: examination_end_frame.id,
		
		place: PLACE_NONE,
		
		examine_conversations: [new Object({
			area: null,
			start: examination_examine_others_first_frame.id,
			end: examination_examine_others_return_frame.id
		})],
		
		deduce_conversations: [new Object({
			area: null,
			elt: null,
			start: examination_deduce_others_first_frame.id,
			end: examination_deduce_others_return_frame.id
		})],
		
		enable_deduction: false
	});
	
	examination_examine_frame.action_name = 'ExaminationExamine';
	examination_examine_frame.action_parameters = prefixRawParameters(new Object({
		context: new Object({
			examination: examination_descriptor
		})
	}));
	
	examination_examine_others_first_frame.wait_time = 1;
	
	examination_examine_others_return_frame.wait_time = 1;
	examination_examine_others_return_frame.action_name = 'GoTo';
	examination_examine_others_return_frame.action_parameters = prefixRawParameters(new Object({
		global: new Object({
			target: examination_examine_frame.id
		})
	}));
	
	examination_deduce_others_first_frame.wait_time = 1;
	
	examination_deduce_others_return_frame.wait_time = 1;
	examination_deduce_others_return_frame.action_name = 'GoTo';
	examination_deduce_others_return_frame.action_parameters = prefixRawParameters(new Object({
		global: new Object({
			target: examination_examine_frame.id
		})
	}));
	
	return newExamination;
}

function createNewScene(position)
{
	var scene_id = getNewRowId('scenes');
	
	var scene_first_frame = createAndInsertDataRow('frames', position);
	
	var newDialogue = createNewDialogue(position + 1, 
		new Object({
			scene_type: 'scenes',
			scene_id: scene_id,
			section_type: 'dialogues',
			section_id: 1
		})
	);
	
	var dialogue_end_position = getRowIndexById('frames', newDialogue.end);
	
	var newExamination = createNewExamination(dialogue_end_position + 1, 
		new Object({
			scene_type: 'scenes',
			scene_id: scene_id,
			section_type: 'examinations',
			section_id: 1
		})
	);
	
	var examination_end_position = getRowIndexById('frames', newExamination.end);
	
	var scene_move_frame = createAndInsertDataRow('frames', examination_end_position + 1);
	
	var newScene = new Object({
		id: scene_id,
		name: '',
		hidden: false,
		
		dialogues: [newDialogue],
		current_dialogue: 1,
		
		examinations: [newExamination],
		current_examination: 1,
		
		start: scene_first_frame.id,
		move: scene_move_frame.id,
		end: scene_move_frame.id,
		
		move_list: []
	});
	
	trial_data.scenes.push(newScene);
	
	scene_first_frame.wait_time = 1;
	scene_first_frame.action_name = 'SceneStart';
	scene_first_frame.action_parameters = prefixRawParameters(new Object({
		context: new Object({
			scene: new Object({
				scene_type: 'scenes',
				scene_id: newScene.id,
			})
		})
	}));
	
	scene_move_frame.action_name = 'SceneMove';
	scene_move_frame.action_parameters = prefixRawParameters(new Object({
		context: new Object({
			scene: new Object({
				scene_type: 'scenes',
				scene_id: newScene.id,
			})
		})
	}));
}

/*
Deletion of blocks 
*/
function deleteFramesBlock(block_type, block_id)
{
	var block_index = getRowIndexById(block_type, block_id);
	var block = trial_data[block_type][block_index];
	
	// Delete all frames between start and end of the block
	var start_frame_index = getRowIndexById('frames', block.start);
	var end_frame_index = getRowIndexById('frames', block.end);
	
	trial_data.frames.splice(start_frame_index, 1 + end_frame_index - start_frame_index);
	
	// Delete block data
	trial_data[block_type].splice(block_index, 1);
}

//END OF MODULE
Modules.complete('trial_data');
