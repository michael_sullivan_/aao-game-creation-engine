{
"not_loaded" : "Le procès n'a pas pu être chargé",
"trial_edited_since_save" : "Le procès a été modifé depuis votre sauvegarde. Cela pourrait rendre le jeu incohérent.",
"trial_doesnt_match_save" : "La sauvegarde que vous avez fourni provient d'un autre procès.",
"tech_save_env_not_initialised" : "L'environnement de sauvegarde n'a pas été initialisé correctement. La sauvegarde est désactive. Merci de signaler le bug sur le forum en donnant un lien vers cette page.",


"start" : "Démarrer",
"loading_sounds" : "Chargement des sons :",
"loading_images" : "Chargement des images :",
"end" : "Fin",

"press" : "Attaquer",
"present" : "Présenter",
"examine" : "Examiner",
"move" : "Se déplacer",
"talk" : "Discuter",

"back" : "Retour",



"profiles" : "Profils",
"evidence" : "Preuves",
"select" : "Selectionner",
"check" : "Vérifier",



"player_settings" : "Réglages",
"mute" : "Muet",
"instant_text_typing" : "Affichage du texte instantané",

"player_saves" : "Sauvegardes",
"save_new" : "Nouvelle sauvegarde",
"save_explain" : "Les sauvegardes sont stockées par votre navigateur web. Si vous changez d'ordinateur ou videz le cache de votre navigateur, vous perdrez vos sauvegardes.\n\nPour éviter cela, ou pour partager vos sauvegardes avec d'autres gens, vous pouvez cliquer droit sur le lien de la sauvegarde, copier l'adresse du lien, et la stocker dans un endroit sûr.",
"save_error_game_not_started": "Impossible de sauvegarder avant d'avoir commencé à jouer.",
"save_error_pending_timer": "Impossible de sauvegarder pendant un message avec délai. Veuillez patienter.",
"save_error_frame_typing": "Impossible de sauvegarder pendant que le texte apparaît. Veuillez patienter.",

"player_debug" : "Debogueur",

"debug_status" : "État du jeu",
"frame_id" : "ID du message",
"frame_index" : "Index du message",

"debug_vars" : "Variables",
"add_var" : "Définir une variable",
"var_name" : "Nom de la variable",

"debug_cr" : "Dossier de l'affaire",

"debug_scenes" : "Scènes d'enquête",

"debug_frames" : "Messages",
"show_frame" : "Surveiller un autre message"
}
